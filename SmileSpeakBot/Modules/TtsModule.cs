﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using Microsoft.Extensions.Options;
using RestEase;
using SmileSpeakBot.Configuration;
using SmileSpeakBot.Services;
using SmileSpeakBot.Services.Tts;

namespace SmileSpeakBot.Modules
{
    public class TtsModule : ModuleBase<SocketCommandContext>
    {
        private readonly IOptions<AppConfig> _appconfig;
        private readonly Speaker _speaker;
        private readonly ITts _tts;

        public TtsModule(IOptions<AppConfig> appconfig, Speaker speaker, ITts tts)
        {
            _appconfig = appconfig;
            _speaker = speaker;
            _tts = tts;
        }

        [Command("tts", RunMode = RunMode.Async)]
        public async Task Tts([Remainder] string text)
        {
            var populatedChannels = Context.Guild.VoiceChannels
                .Where(ch => ch.Users.Count != 0).ToImmutableList();

            if (populatedChannels.Count != 0)
            {
                await _speaker.Speak(populatedChannels, await _tts.GetSpeachStream(text));
            }
        }
    }
}
