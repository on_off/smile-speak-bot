﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SmileSpeakBot.Services.RestTypes
{
    public class TranslationResponse
    {
        public class ExtractedData
        {
            public string Translation { get; set; }
            public string ActualQuery { get; set; }
            public int ResultType { get; set; }
            public string Transliteration { get; set; }
            public string[] Synonyms { get; set; }

            [JsonProperty("sourceLanguage")]
            public string SourceLanguageCode { get; set; }
        }

        [JsonProperty("extract")]
        public ExtractedData Extracted { get; set; }

        public string OriginalResponse { get; set; }

    }
}
