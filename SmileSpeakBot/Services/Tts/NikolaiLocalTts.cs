﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using RestEase;
using SmileSpeakBot.Configuration;

namespace SmileSpeakBot.Services.Tts
{
    class NikolaiLocalTts : ITts
    {
        private readonly INikolaiApi _talkerApi;

        public NikolaiLocalTts(IOptions<AppConfig> appconfig)
        {
            _talkerApi = RestClient.For<INikolaiApi>(appconfig.Value.NikolaiSpeech.ApiUri);
        }

        public async Task<Stream> GetSpeachStream(string text)
        {
            var response = await _talkerApi.GetTtsAsync(text);
            return await FFmpegWraper.GetPcmStreamAsync(
                await response.Content.ReadAsStreamAsync());
        }
    }
}
