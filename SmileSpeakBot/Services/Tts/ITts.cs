﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace SmileSpeakBot.Services.Tts
{
    public interface ITts
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">Text to be spoken</param>
        /// <returns>PCM stream, ready to be broadcasted</returns>
        Task<Stream> GetSpeachStream(String text);
    }
}
