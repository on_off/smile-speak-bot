﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using RestEase;
using SmileSpeakBot.Services.RestTypes;

namespace SmileSpeakBot.Services
{
    public interface ITranslateProxyApi
    {
        [Get("languages")]
        Task<List<Language>> GetLanguageListAsync();

        [Get("translate")]
        Task<TranslationResponse> GetTranslationAsync(string query, string targetLang, string sourceLang = "auto");

        [Get("tts")]
        Task<HttpResponseMessage> GetTtsAsync(string query, string language, [Range(0.2, 1.0)]double speed);
    }
}
