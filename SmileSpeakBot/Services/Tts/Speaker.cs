﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;

namespace SmileSpeakBot.Services.Tts
{
    public class Speaker
    {
        private static readonly SemaphoreSlim SpeakingMutex = new SemaphoreSlim(1);

        public async Task Speak(IReadOnlyCollection<IVoiceChannel> channels, Stream audio)
        {
            await SpeakingMutex.WaitAsync();
            try
            {
                foreach (var voiceChannel in channels)
                {
                    var noUsers = await voiceChannel
                        .GetUsersAsync()
                        .All(list => list.Count == 0); //GetUsersAsync returns ENUMERATION of COLLECTIONS of USERS.
                    if (noUsers) continue;               //Bullshit design at it's finest
                    try
                    {
                        using (var client = await voiceChannel.ConnectAsync())
                        {
                            using (var stream = client.CreatePCMStream(AudioApplication.Voice, 1920, 100))
                            {
                                audio.Position = 0;
                                await audio.CopyToAsync(stream);
                                await stream.FlushAsync().ConfigureAwait(false);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            finally
            {
                SpeakingMutex.Release();
                audio.Dispose();
            }
        }
    }
}