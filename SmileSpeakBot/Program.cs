﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;
using Discord.WebSocket;
using Discord.Commands;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SmileSpeakBot.Configuration;
using SmileSpeakBot.Services;
using SmileSpeakBot.Services.Tts;

namespace SmileSpeakBot
{
    public class Program
    {
        // Convert our sync main to an async main.
        public static void Main(string[] args) => new Program().Start().GetAwaiter().GetResult();
        
        private DiscordSocketClient _client;
        public static IConfigurationRoot Configuration { get; set; }

        public async Task Start()
        {
            // Grabbing settings
            ConfigurationBuilder configurationBuilder =
                new ConfigurationBuilder();

            configurationBuilder
                .AddJsonFile("config.json", false)
                .AddUserSecrets<Program>();

            Configuration = configurationBuilder.Build();
            
            // Define the DiscordSocketClient
            _client = new DiscordSocketClient();

            // Config and start services
            var services = ConfigureServices();
            services.GetRequiredService<LogService>();
            services.GetRequiredService<Speaker>();
            await services.GetRequiredService<CommandHandlingService>().InitializeAsync(services);
            
            // Login and connect to Discord.
            await _client.LoginAsync(TokenType.Bot, Configuration["Discord:Token"]);
            await _client.StartAsync();
            
            // Block this program until it is closed.
            await Task.Delay(-1);
        }

        private IServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                // Base
                .AddSingleton(_client)
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .AddSingleton<Speaker>()
                .AddTransient<ITts, NikolaiLocalTts>()
                // Logging
                .AddLogging()
                .AddSingleton<LogService>()
                // Settings
                .AddOptions()
                .Configure<AppConfig>(Configuration)
                // Add additional services here...
                .BuildServiceProvider();
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
