﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmileSpeakBot.Services.RestTypes
{
    public class Language
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
