﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using RestEase;

namespace SmileSpeakBot.Services
{
    public interface INikolaiApi
    {
        [Get("tts")]
        Task<HttpResponseMessage> GetTtsAsync(string text);
    }
}
