﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using RestEase;
using SmileSpeakBot.Configuration;

namespace SmileSpeakBot.Services.Tts
{
    public class GoogleTts : ITts
    {
        private readonly ITranslateProxyApi _translator;
        private readonly IOptions<AppConfig> _appconfig;

        public GoogleTts(IOptions<AppConfig> appconfig)
        {
            _appconfig = appconfig;
            _translator = RestClient.For<ITranslateProxyApi>(_appconfig.Value.Speech.ApiUri);
        }

        public async Task<Stream> GetSpeachStream(string text)
        {
            var translation = await _translator.GetTranslationAsync(text, "en");
            string lang = translation.Extracted.SourceLanguageCode ?? "ru";
            var response = await _translator.GetTtsAsync(text, lang, 1.0);
            return await FFmpegWraper.GetPcmStreamAsync(
                await response.Content.ReadAsStreamAsync());
        }
    }
}
