﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SmileSpeakBot.Services
{
    class FFmpegWraper
    {
        private static Process CreateFfmpegProcess()
        {
            var ffmpegStartInfo = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-i pipe:0 -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardInput = true
                // CreateNoWindow = true
            };
            Process ffmpeg = new Process
            {
                StartInfo = ffmpegStartInfo,
                EnableRaisingEvents = true
            };
            ffmpeg.Start();
            return ffmpeg;
        }

        public static async Task<Stream> GetPcmStreamAsync(Stream content)
        {
            MemoryStream memSound;
            using (var ffmpeg = CreateFfmpegProcess())
            {
                var output = ffmpeg.StandardOutput.BaseStream;
                Task<MemoryStream> task = Task<MemoryStream>.Factory.StartNew(() =>
                {
                    var ms = new MemoryStream();
                    output.CopyTo(ms);
                    return ms;
                });
                using (var inpStream = ffmpeg.StandardInput.BaseStream)
                {
                    await content.CopyToAsync(inpStream);
                    await inpStream.FlushAsync();
                }
                task.Wait();
                memSound = task.Result;
                output.Dispose();
            }
            return memSound;
        }
    }
}
