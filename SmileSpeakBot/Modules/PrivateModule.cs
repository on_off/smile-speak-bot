﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;

namespace SmileSpeakBot.Modules
{
    [Group("manage")]
    [RequireOwner]
    public class PrivateModule : ModuleBase<SocketCommandContext>
    {
        //!manage play <game>
        [Command("play")]
        [Alias("game")]
        [Summary("Set the game bot plays")]
        public async Task SetGame([Remainder] string input)
        {
            await Context.Client.SetGameAsync(input);
            await ReplyAsync("OK Boss.");
        }

        [Command("name")]
        [Summary("Set the name of the bot")]
        public async Task SetName([Remainder] string input)
        {
            await Context.Client.CurrentUser.ModifyAsync(properties => properties.Username = input);
            await ReplyAsync("OK Boss.");
        }
    }
}
