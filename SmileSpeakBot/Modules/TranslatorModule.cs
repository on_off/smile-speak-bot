﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;
using Discord.Commands;
using Microsoft.Extensions.Options;
using RestEase;
using SmileSpeakBot.Configuration;
using SmileSpeakBot.Services;
using SmileSpeakBot.Services.RestTypes;
using SmileSpeakBot.Services.Tts;

namespace SmileSpeakBot.Modules
{
    public class TranslatorModule : ModuleBase<SocketCommandContext>
    {
        private readonly IOptions<AppConfig> _appconfig;
        private readonly ITranslateProxyApi _translator;

        public TranslatorModule(IOptions<AppConfig> appconfig)
        {
            _appconfig = appconfig;
            _translator = RestClient.For<ITranslateProxyApi>(_appconfig.Value.Speech.ApiUri);
        }

        [Command("translate")]
        [Alias("t8")]
        [Summary("Translates, duh. Syntax is !t8 ru text")]
        public async Task TranslateAuto(string langcode, [Remainder] string text)
        {
            TranslationResponse translation;
            try
            {
                translation = await _translator.GetTranslationAsync(text, langcode);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await Task.Delay(300);
                try
                {
                    translation = await _translator.GetTranslationAsync(text, langcode);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    await ReplyAsync("Something went wrong");
                    return;
                }
            }
            await ReplyAsync(
                $"{Format.Bold(translation.Extracted.Translation)}\n"+
                $"{Format.Italics("Source language:")} {translation.Extracted.SourceLanguageCode}");
        }

        [Command("languages")]
        public async Task ListLanguages()
        {
            var languages = await _translator.GetLanguageListAsync();
            string reply = $"{Format.Bold("Name")}\t {Format.Bold("Code")}\n";
            foreach (var a in languages)
            {
                reply += $"{a.Name}:\t {a.Code}\n";
            }
            await ReplyAsync(reply);
        }
    }
}
