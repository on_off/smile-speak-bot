﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmileSpeakBot.Configuration
{
    public class AppConfig
    {
        private Dictionary<char, string> _preparedDictionary;

        public class DiscordConfig
        {
            public string Token { get; set; }
        }

        public class SpeechConfig
        {
            public string ApiUri { get; set; }
        }

        public class NikolaiSpeechConfig
        {
            public string ApiUri { get; set; }
        }


        public NikolaiSpeechConfig NikolaiSpeech { get; set;}
        public DiscordConfig Discord { get; set; }
        public SpeechConfig Speech { get; set; }

        public Dictionary<string, string> ReplacementDictionary { get; set; }

        public Dictionary<char, string> PreparedDictionary
        {
            get
            {
                if (_preparedDictionary == null)
                {
                    _preparedDictionary = new Dictionary<char, string>();
                    foreach (var e in ReplacementDictionary)
                    {
                        _preparedDictionary.Add(e.Key.First(), e.Value);
                    }
                }
                return _preparedDictionary;
            }
        }
    }
}
